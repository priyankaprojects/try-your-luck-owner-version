//
//  APIManager.swift
//  Try Your Luck Owner
//
//  Created by Hitesh on 04/01/19.
//  Copyright © 2019 Hitesh Agarwal. All rights reserved.
//

import UIKit

class APIManager: NSObject {
    typealias CallBackHandler = (_ data: Any?, _ message: String?, _ error: Error?) -> Void
    
    //MARK: Save Details of Owner
    public class func saveOwnerDetails(device_token: String, store_name: String, owner_email: String, handler: @escaping CallBackHandler){
        let url = baseUrl + "owner_details_insert?"
        let param = "device_token=\(device_token)&device_type=iOS&store_name=\(store_name)&owner_email=\(owner_email)"
        let method = "POST"
        
        let restClient = SHRestClient(withUrl: url, Body: param, Method: method, Headers: nil)
        restClient.sendParamswithCompletion({(object, error) in
            if error == nil{
                let response = object as! NSDictionary
                let message = response["message"] as! String
                let success = response["success"] as! Bool
                
                if success {
                    
                    handler(response["data"], message, nil)
                }else{
                    handler(nil, message, nil)
                }
            }else{
                handler(nil, nil, error)
            }
        })
    }
    
    /*public class func fetchOffers(handler: @escaping CallBackHandler){
        let url = baseUrl + "offer_listing?"
        let param = "device_token=12345678".replacingOccurrences(of: " ", with: "%20")
        let method = "POST"
        
        NetworkHandler.handleRequest(url: url, methodName: method, parameters: param, showLoader: true, completion: {(result) in
            
            let response = result as NSDictionary
            let success = response.value(forKey: "success") as! Bool
            let message = response.value(forKey: "message") as! String
            
            if success {
                let data = response.value(forKey: "data") as? NSArray
                if data != nil{
                    handler(data, message, nil)
                }else{
                    handler(nil, message, nil)
                }
            }else{
                handler(nil, nil, response)
            }
        })
    }*/
    //MARK: Daily Offer Set
    public class func setOffer(handler: @escaping CallBackHandler){
        let url = baseUrl + "set_daily_offer?"
        let parameter = "set_offer_amount=100&maximum_offer_value=8&device_token=12345678"
        let method = "POST"
        
        let restClient = SHRestClient(withUrl: url, Body: parameter, Method: method, Headers: nil)
        restClient.sendParamswithCompletion({(object, error) in
            if error == nil{
                let response = object as! NSDictionary
                let message = response["message"] as! String
                let success = response["success"] as! Bool
                
                if success {
                    //handler(success, message, nil)
                }else{
                    handler(nil, message, nil)
                }
            }else{
                handler(nil, nil, error)
            }
        })
    }
    //MARK: Offer Value Update
    //http://techexactly.info/Clients/custom/tryyourluck/api/update_daily_offer?offer_set_id=10&offer_amount_set=20
    public class func updateOffer(handler: @escaping CallBackHandler){
        let url = baseUrl + "update_daily_offer?"
        let parameter = "offer_set_id=10&offer_amount_set=20"
        let method = "POST"
        
        let restClient = SHRestClient(withUrl: url, Body: parameter, Method: method, Headers: nil)
        restClient.sendParamswithCompletion({(object, error) in
            if error == nil{
                let response = object as! NSDictionary
                let message = response["message"] as! String
                let success = response["success"] as! Bool
                
                if success {
                    //handler(success, message, nil)
                }else{
                    handler(nil, message, nil)
                }
            }else{
                handler(nil, nil, error)
            }
        })
    }
    //MARK: Offer List Fetch
    public class func fetchOffers(handler: @escaping CallBackHandler){
        let url = baseUrl + "offer_listing?"
        let param = "device_token=12345678"
        let method = "POST"
        
        let restClient = SHRestClient(withUrl: url, Body: param, Method: method, Headers: nil)
        restClient.sendParamswithCompletion({(object, error) in
            
            if error == nil {
                let response = object as! NSDictionary
                let message = response["message"] as! String
                let success = response["success"] as! Bool
                
                if success {
                    let responseData = NSMutableArray()
                    let data = response["data"] as! NSArray
                    
                    
                    data.enumerateObjects({(obj, index, stop) in
                        let model = OfferModel(dict: obj as! NSDictionary)
                        responseData.add(model)
                    })
                    
                    handler(responseData, message, nil)
                }else{
                    handler(nil, message, nil)
                }
            }else{
                handler(nil, nil, error)
            }
        })
    }
    
    //MARK: Upload Image
    public class func uploadImage(param: String, handler: @escaping CallBackHandler){
        let url = baseUrl + "barcode_image_upload?"
        let parameter = param
        let method = "POST"
        
        let restClient = SHRestClient(withUrl: url, Body: parameter, Method: method, Headers: nil)
        restClient.sendParamswithCompletion({(object, error) in
            if error == nil{
                let response = object as! NSDictionary
                let message = response["message"] as! String
                let success = response["success"] as! Bool
                
                if success {
                    handler(success, message, nil)
                }else{
                    handler(nil, message, nil)
                }
            }else{
                handler(nil, nil, error)
            }
        })
    }
    
    
    //MARK: Fetch
    public class func fetchBarcode(storeName: String, email: String, handler: @escaping CallBackHandler){
        let url = baseUrl + "fetch_barcode_image?"
        let parameter = "store_name=\(storeName)&owner_email=\(email)"
        let method = "POST"
        
        let restClient = SHRestClient(withUrl: url, Body: parameter, Method: method, Headers: nil)
        restClient.sendParamswithCompletion({(object, error) in
            if error == nil{
                let response = object as! NSDictionary
                let message = response["message"] as! String
                let success = response["success"] as! Bool
                
                if success {
                    let responseData = response["data"] as! NSDictionary
                    let imgUrl = responseData["barcode_image_url"] as! String
                    handler(imgUrl, message, nil)
                }else{
                    handler(nil, message, nil)
                }
            }else{
                handler(nil, nil, error)
            }
        })
    }
    
}
