//
//  NetworkHandler.swift
//  BarBuddy
//
//  Created by Hitesh on 26/09/18.
//  Copyright © 2018 Hitesh. All rights reserved.
//

import Foundation

typealias Completion = (_ dictData: NSDictionary) -> Void


class NetworkHandler: NSObject {
    
    //MARK: HANDLE REQUESTS
    class func handleRequest(url: String, methodName: String, parameters: String, showLoader: Bool, completion: @escaping Completion){
        
        WebService.callWebServices(url: url, methodName: methodName, parameters: parameters, showLoader: showLoader) { (response) in
            
            DispatchQueue.main.async {
                completion(response)
            }
            
        }
    }
}



