//
//  WebService.swift
//  SSA
//
//  Created by Hitesh on 13/03/18.
//

import Foundation

typealias complition_dictDAta = (_ data: NSDictionary) -> Void
typealias CompletionHandler = (_ dictData: NSDictionary) -> Void
typealias CompletionHandler_Backup = (_ dictData: NSDictionary) -> Void
typealias CompletionHandler2 = (_ success: Bool ,_ dictData: NSDictionary) -> Void

class WebService: NSObject {
    
    public override init() {
        
    }
    
    // MARK: Calling Web services
    class func callWebServices(url: String, methodName: String, parameters: String, showLoader: Bool, completion: @escaping CompletionHandler){
        
        var url_param = url
        
        if Reachability.isInternetAvailable() {
            
            if showLoader{
                KVNProgress.show(withStatus: "Loading...")
            }
            
        } else {
            completion(NSDictionary())
            KVNProgress.showError(withStatus: "Your Internet connection seems to be Offline.")
        }
        
        if(methodName == "GET" && parameters != "")
        {
            url_param = url_param + "?" + parameters
        }
        
        var request = URLRequest(url: URL(string: url_param)!)
        request.httpMethod = methodName
        
        if(methodName == "POST" && parameters != "")
        {
            let postString = parameters
            request.httpBody = postString.data(using: .utf8)
        }
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            guard let data = data, error == nil else {
                print("error = \(String(describing: error))")
                KVNProgress.dismiss()
                return
            }
            
            if let httpsStatus = response as? HTTPURLResponse, httpsStatus.statusCode != 200 {
                print("Status Code should be 200, but it is \(httpsStatus.statusCode)")
                print("response = \(String(describing: response))")
                completion(NSDictionary())
                KVNProgress.dismiss()
            }
            
            do {
                let dictData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! NSDictionary
                
                
                DispatchQueue.main.async {
                    print("Json Result :\(dictData)")
                    KVNProgress.dismiss()
                    completion(dictData)
                }
                
            } catch {
                KVNProgress.showError(withStatus: "\(error.localizedDescription)")
                
                completion(NSDictionary())
                print("error is : \(error)")
            }
        }
        task.resume()
    }
    
    
    // MARK: Calling Web services
    class func callWebServices2(url: String, methodName: String, parameters: String, showLoader: Bool, completion: @escaping CompletionHandler2){
        
        var url_param = url
        
        if Reachability.isInternetAvailable() {
            
            if showLoader{
                KVNProgress.show(withStatus: "Loading...")
            }
            
        } else {
            completion(false, NSDictionary())
            KVNProgress.showError(withStatus: "Your Internet connection seems to be Offline.")
        }
        
        if(methodName == "GET" && parameters != "")
        {
            url_param = url_param + "?" + parameters
        }
        
        var request = URLRequest(url: URL(string: url_param)!)
        request.httpMethod = methodName
        
        if(methodName == "POST" && parameters != "")
        {
            let postString = parameters
            request.httpBody = postString.data(using: .utf8)
        }
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            guard let data = data, error == nil else {
                print("error = \(String(describing: error))")
                KVNProgress.dismiss()
                return
            }
            
            if let httpsStatus = response as? HTTPURLResponse, httpsStatus.statusCode != 200 {
                print("Status Code should be 200, but it is \(httpsStatus.statusCode)")
                print("response = \(String(describing: response))")
                completion(false, NSDictionary())
                KVNProgress.dismiss()
            }
            
            do {
                let dictData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! NSDictionary
                
                DispatchQueue.main.async {
                    print("Json Result :\(dictData)")
                    KVNProgress.dismiss()
                    completion(true, dictData)
                }
                
            } catch {
                KVNProgress.showError(withStatus: "\(error.localizedDescription)")
                
                completion(false, NSDictionary())
                print("error is : \(error)")
            }
        }
        task.resume()
    }
    
    
    //MARK: Download Data From Url
    class func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
}

