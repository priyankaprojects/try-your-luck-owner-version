//
//  Extensions.swift
//  Try Your Luck Owner
//
//  Created by Hitesh Agarwal on 20/12/18.
//  Copyright © 2018 Hitesh Agarwal. All rights reserved.
//

import UIKit

extension UIColor {
    
    static let appPrimaryPurple        =   UIColor(hex: 0x6E00D5)
    static let appPrimaryDarkPurple    =   UIColor(hex: 0x6400C2)
    static let appDisabledButtonText   =   UIColor(hex: 0xD1D1D1)
    static let appGradientCyan         =   UIColor(hex: 0x1AE3D9)
    static let appGradientIndigo       =   UIColor(hex: 0x5C7AEA)
    
    /**
     init with RGB color codes (Hexadecimal)
     */
    convenience init(hex: UInt) {
        self.init(red    : CGFloat((hex & 0xFF0000) >> 16) / 255,
                  green  : CGFloat((hex & 0x00FF00) >> 8) / 255,
                  blue   : CGFloat(hex & 0x0000FF) / 255,
                  alpha  : 1.0)
    }
    
    /**
     init with RGBA color codes (Hexadecimal)
     */
    convenience init(hexCode: UInt) {
        self.init(red    : CGFloat((hexCode & 0xFF000000) >> 24) / 255,
                  green  : CGFloat((hexCode & 0x00FF0000) >> 16) / 255,
                  blue   : CGFloat((hexCode & 0x0000FF00) >> 8) / 255,
                  alpha  : CGFloat(hexCode & 0x000000FF) / 255)
    }
}

extension UIApplication {
    var statusBar: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
    
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}

extension UIView {
    
    public func applyGradient(withColors colors: [UIColor],
                              startPoint sp: CGPoint,
                              endPoint ep: CGPoint,
                              locations loc: [NSNumber]?) {
        layer.masksToBounds = true
        layer.cornerRadius = 5.0
        let gradientLayer = CAGradientLayer()
        var interNalColorArray = [CGColor]()
        
        for i in colors {
            interNalColorArray.append(i.cgColor)
        }
        
        gradientLayer.frame = bounds
        gradientLayer.colors = interNalColorArray
        gradientLayer.startPoint = sp
        gradientLayer.endPoint = ep
        gradientLayer.locations = loc
        
        layer.insertSublayer(gradientLayer, at: 0)
        
        self.addObserver(self, forKeyPath: "bounds", options: [.old, .new], context: nil)
    }
    
    
    
    open override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "bounds" {
            for i in self.layer.sublayers! {
                if i is CAGradientLayer {
                    i.frame = change![NSKeyValueChangeKey.newKey] as! CGRect
                }
            }
        }
    }
}
