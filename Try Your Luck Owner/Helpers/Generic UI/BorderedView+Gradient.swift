//
//  BorderedView.swift
//  Try Your Luck Owner
//
//  Created by Hitesh Agarwal on 20/12/18.
//  Copyright © 2018 Hitesh Agarwal. All rights reserved.
//

import UIKit

class BorderedView: UIView {

    typealias RGBA = UIColor
    
    fileprivate var _BGColor: RGBA = UIColor(hexCode: 0xFFFFFFFF)
    public var BGColor: RGBA? {
        get {
            return _BGColor
        }
        set {
            _BGColor = newValue!
        }
    }
    
    fileprivate var _cRadius: CGFloat = 5.0
    public var cornerRadius: CGFloat? {
        get {
            return _cRadius
        }
        set {
            _cRadius = newValue!
        }
    }
    
    fileprivate var _bWidth: CGFloat = 0.5
    public var borderWidth: CGFloat? {
        get {
            return _bWidth
        }
        set {
            _bWidth = newValue!
        }
    }
    
    fileprivate var _bColor: RGBA = UIColor(hexCode: 0xBDBDBDFF)
    public var borderColor: RGBA? {
        get {
            return _bColor
        }
        set {
            _bColor = newValue!
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setBorder()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setBorder()
    }
    
    private func setBorder() {
        
        self.backgroundColor     =   BGColor!
        self.layer.cornerRadius  =   cornerRadius!
        self.layer.borderColor   =   borderColor?.cgColor
        self.layer.borderWidth   =   borderWidth!
    }
}


class GradientView: UIView {
    
    public static let shared = GradientView()
    public var colorArray = [UIColor.appGradientIndigo, UIColor.appGradientCyan] {
        didSet {
            applyGradient()
        }
    }
    let startPoint = CGPoint(x: 0, y: 0)
    let endPoint = CGPoint(x: 1, y: 0)
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        applyGradient()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        applyGradient()
    }
    
    func applyGradient() {
        self.applyGradient(withColors: colorArray, startPoint: startPoint, endPoint: endPoint, locations: nil)
    }
    
    override func layoutSubviews() {
        //self.applyGradient(withColors: colorArray, startPoint: startPoint, endPoint: endPoint, locations: nil)
    }
}
