//
//  SubmitButton.swift
//  Try Your Luck Owner
//
//  Created by Hitesh Agarwal on 20/12/18.
//  Copyright © 2018 Hitesh Agarwal. All rights reserved.
//

import UIKit

class SubmitButton: UIButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.titleLabel?.font = UIFont(name: "SFProDisplay-Semibold", size: 20)
        self.setTitleColor(.white, for: .normal)
        self.backgroundColor = UIColor.appPrimaryPurple
        self.layer.cornerRadius = 5.0
        
        self.layer.shadowColor = UIColor.appPrimaryPurple.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowRadius = 8
        self.layer.shadowOpacity = 0.7
    }
    
}
