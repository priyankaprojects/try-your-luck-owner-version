//
//  TableViewUIController.swift
//  Try Your Luck Owner
//
//  Created by Hitesh Agarwal on 20/12/18.
//  Copyright © 2018 Hitesh Agarwal. All rights reserved.
//

import UIKit

@objc protocol tappedOnTableCell {
    @objc func rowSelected(withIndexPath: IndexPath)
    @objc func rowDeselected(withIndexPath: IndexPath)
}

@objc protocol customGesturesOnTableView: UIGestureRecognizerDelegate {
    
    @objc optional func recognizedTapOnTable(state: UITapGestureRecognizer.State)
    @objc optional func recognizedLongPressOnTable(state: UILongPressGestureRecognizer.State)
}

class TableViewController: NSObject {
    
    public static let `default` = TableViewController()
    public var sectionNumber: Int?
    public var numberOfRowsInSection: [String:Int]?
    public var cellIdentifier: String!
    public var delegate: tappedOnTableCell?
    public var loadData: NSArray?
    public var rowHeight: CGFloat?
}

extension TableViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionNumber == nil ? 0 : sectionNumber ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (sectionNumber == nil || loadData == nil) ? 0 : numberOfRowsInSection?["\(section)"] ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if cellIdentifier != nil {
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! UITableViewCellController
            cell.loadData = self.loadData?[indexPath.row] as? NSDictionary
            return cell
        } else {
            return UITableViewCell()
        }
    }
}

extension TableViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.rowSelected(withIndexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        delegate?.rowDeselected(withIndexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rowHeight ?? 0
    }
}

extension TableViewController {
    
}
