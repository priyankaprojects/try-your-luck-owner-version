//
//  HamburgerVC.swift
//  Try Your Luck Owner
//
//  Created by Hitesh Agarwal on 20/12/18.
//  Copyright © 2018 Hitesh Agarwal. All rights reserved.
//

import UIKit

class HamburgerVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    //MARK: Outlate
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var menuTable: UITableView!
    @IBOutlet weak var yourBarcodeIV: UIImageView!
    @IBOutlet weak var lbl_storeName: UILabel!
    @IBOutlet weak var lbl_storeEmail: UILabel!
    
    
    //MARK: VAR
    lazy var cancelButton = UIButton(frame: .zero)
    
    let item1 = Hamburger.HamburgerItems(id: "1", menuName: "My History", menuImage: #imageLiteral(resourceName: "history-ic"))
    var menuItems: [Hamburger.HamburgerItems]!
    private var barCodeImageURL = AppManager.Barcode.barcodeImageURL
    private var barcodeImage = AppManager.Barcode.barcodeImage
    
    
    //MARK: Action
    @IBAction func action_test(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    //MARK: Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //setBarcodeImage()
        setupInitialView()
        //setupTable()
        animateToFinalPosition()
        
        self.lbl_storeName.text = (UserDefaults.standard.value(forKey: "storeName") as! String)
        self.lbl_storeEmail.text = (UserDefaults.standard.value(forKey: "email") as! String)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func setBarcodeImage() {
        let fileManager = FileManager.default
        let imagePath = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(barcoodeImageName)
        if fileManager.fileExists(atPath: imagePath) {
            let i = UIImage(contentsOfFile: imagePath)
            yourBarcodeIV.image = i
        } else {
            yourBarcodeIV.image = barcodeImage
        }
    }
    
    @objc func setupInitialView() {
        self.view.frame.origin = CGPoint(x: -self.view.frame.width, y: 0)
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        
    }
    
    @objc func animateToFinalPosition() {
        
        UIView.animate(withDuration: 1.0, animations: {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        })
        
        UIView.animate(withDuration: 0.57, delay: 0.0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.9, options: .curveEaseIn, animations: {
            self.view.frame.origin = CGPoint(x: 0, y: 0)
        }) { _ in
            self.cancelButton.frame = self.view.frame
            self.view.insertSubview(self.cancelButton, belowSubview: self.baseView)
            self.cancelButton.addTarget(self, action: #selector(self.test), for: .touchUpInside)
        }
    }
    
//    @objc func setupTable() {
//        menuItems = [item1]
//        AppManager.hamburger.hamburgerItems = menuItems
//
//        menuTable.delegate = AppManager.tableController
//        menuTable.dataSource = AppManager.tableController
//        var e: [NSDictionary] = []
//        for i in menuItems {
//            e.append(i.hamDict)
//        }
//        AppManager.tableController.loadData = e as NSArray
//        AppManager.tableController.cellIdentifier = "MenuCell"
//        AppManager.tableController.sectionNumber = 1
//        AppManager.tableController.numberOfRowsInSection = ["0":AppManager.hamburger.hamburgerItems.count]
//        AppManager.tableController.rowHeight = 90
//        AppManager.tableController.delegate = self
//        menuTable.reloadData()
//    }
    
    @objc func removeSelf() {
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        
        UIView.animate(withDuration: 0.57, delay: 0.0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.9, options: .curveEaseIn, animations: {
            self.view.frame.origin = CGPoint(x: -self.view.frame.width, y: 0)
        }) { _ in
//            self.cancelButton.frame = .zero
//            self.cancelButton.removeTarget(self, action: #selector(self.removeSelf), for: .touchUpInside)
            //self.view.removeFromSuperview()
        }
        
    }
    @objc func test(){
        self.dismiss(animated: true, completion: nil)
    }
        
    
    //MARK: Delegate & DataSource
    //Table view delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return 3
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "BarCodeTVCell", for: indexPath) as! BarCodeTVCell
            
            cell.loadCell()
            
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTVCell", for: indexPath) as! MenuTVCell
            
            cell.loadCell(row: indexPath.row, handler: {(data, message) in
                
            })
            
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    
}

/*extension HamburgerVC: tappedOnTableCell {
    
    func rowSelected(withIndexPath: IndexPath) {
        if let _ = menuTable.cellForRow(at: withIndexPath) as? HamburgerTVC {
            let id = menuItems[withIndexPath.row].id
            if id == "1" {
                self.removeSelf()
                let go = mainStoryboard.instantiateViewController(withIdentifier: "OfferHistoryVC") as! OfferHistoryVC
                let topVC = UIApplication.topViewController()
                topVC?.navigationController?.pushViewController(go, animated: true)
                
            }
        }
    }
    
    func rowDeselected(withIndexPath: IndexPath) {
        
    }
}*/
