//
//  HamburgerTVC.swift
//  Try Your Luck Owner
//
//  Created by Hitesh Agarwal on 20/12/18.
//  Copyright © 2018 Hitesh Agarwal. All rights reserved.
//

import UIKit

class HamburgerTVC: UITableViewCellController {

    @IBOutlet weak var menuImageIV: UIImageView!
    @IBOutlet weak var menuName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
        
        if loadData != nil {
            menuImageIV.image = loadData?.value(forKey: "menuImage") as? UIImage
            menuName.text = loadData?.value(forKey: "menuName") as? String
        }
    }
    
    
}
