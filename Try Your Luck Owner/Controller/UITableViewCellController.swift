//
//  UITableViewCellController.swift
//  Try Your Luck Owner
//
//  Created by Hitesh Agarwal on 20/12/18.
//  Copyright © 2018 Hitesh Agarwal. All rights reserved.
//

import UIKit

class UITableViewCellController: UITableViewCell {
    
    public var loadData: NSDictionary? {
        didSet {
            awakeFromNib()
            setupElements()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupElements() {
        
    }
}
