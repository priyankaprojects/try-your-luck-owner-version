//
//  TutorialCVCell.swift
//  Try Your Luck Owner
//
//  Created by Hitesh on 02/01/19.
//  Copyright © 2019 Hitesh Agarwal. All rights reserved.
//

import UIKit

class TutorialCVCell: UICollectionViewCell {
    //MARK:- Outlate
    @IBOutlet weak var img_top: UIImageView!
    @IBOutlet weak var btn_skip: UIButton!
    @IBOutlet weak var btn_continue: UIButton!
    
    @IBOutlet weak var lbl_heading: UILabel!
    @IBOutlet weak var lbl_description: UILabel!
    
    
    //MARK:- VAR
    typealias CellHandler = (_ data: Any?, _ message: String) -> Void
    var handler : CellHandler!
    
    
    //MARK:- Action
    @IBAction func action_skipBtn(_ sender: Any) {
        handler(nil,"skip")
    }
    
    @IBAction func action_continueBtn(_ sender: Any) {
        handler(nil,"continue")
    }
    
    
    //MARK:- Method
    func loadCell(itemNumber: Int, handler: @escaping CellHandler){
        self.handler = handler
        switch itemNumber {
        case 0:
            self.img_top.image = UIImage(named: "generate-barcode")
        case 1:
            self.img_top.image = UIImage(named: "print-baecode")
        case 2:
            self.img_top.image = UIImage(named: "set-offer")
        default:
            print("Switch_Default")
        }
        if itemNumber == 2{
            self.btn_continue.isHidden = false

            self.btn_skip.isHidden = true
            
            /*UIView.transition(with: btn_skip, duration: 0.5, options: .curveEaseIn, animations: {
                self.btn_skip.alpha = 0.0
                self.btn_continue.alpha = 1.0
            }, completion: nil)*/
        }else{
            self.btn_continue.isHidden = true

            self.btn_skip.isHidden = false
            /*UIView.transition(with: btn_skip, duration: 0.5, options: .curveEaseIn, animations: {
                self.btn_skip.alpha = 1.0
                self.btn_continue.alpha = 0.0
            }, completion: nil)*/
            
        }
    }
}
