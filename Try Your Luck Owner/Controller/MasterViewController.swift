//
//  MasterViewController.swift
//  Try Your Luck Owner
//
//  Created by Hitesh Agarwal on 20/12/18.
//  Copyright © 2018 Hitesh Agarwal. All rights reserved.
///

import UIKit

class MasterViewController: UIViewController {

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    enum leftButtonType {
        case menu
        case back
    }
    
    enum notificationColor {
        case light
        case dark
    }
    
    lazy var headerView = UIView()
    lazy var titleLabel = UILabel()
    lazy var navLeftButton = UIButton()
    lazy var notificationTxtV = UITextView()
    var notificationTxtVColor: notificationColor? {
        didSet {
            setNotificationTxtVColors(forColorScheme: notificationTxtVColor!)
        }
    }
    var notificationTxtVHeight: NSLayoutConstraint!
    var notificationTxtVBottom: NSLayoutConstraint!
    
    var titleText: String? {
        didSet {
            self.changeTitleText()
        }
    }
    
    var navLeftButtonType: leftButtonType? {
        didSet {
            self.changeLeftButtonType()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setHeader()
        setNotificationTxtV()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBar?.backgroundColor = UIColor.appPrimaryDarkPurple
    }
    
    func setHeader() {
        headerView = UIView(frame: .zero)
        headerView.translatesAutoresizingMaskIntoConstraints = false
        headerView.backgroundColor = UIColor.appPrimaryPurple
        view.addSubview(headerView)
        let safeAreaTop = CommonMethods.safeAreaTopHeight()
        let constraintsHeaderView = [headerView.topAnchor.constraint(equalTo: view.topAnchor),
                                     headerView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                                     headerView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                                     headerView.heightAnchor.constraint(equalToConstant: safeAreaTop + 50)]
        NSLayoutConstraint.activate(constraintsHeaderView)
        
        navLeftButton.imageEdgeInsets = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
        navLeftButton.translatesAutoresizingMaskIntoConstraints = false
        headerView.addSubview(navLeftButton)
        let constraintsNavLeftButton = [navLeftButton.leadingAnchor.constraint(equalTo: headerView.leadingAnchor),
                                        navLeftButton.bottomAnchor.constraint(equalTo: headerView.bottomAnchor),
                                        navLeftButton.widthAnchor.constraint(equalToConstant: 50),
                                        navLeftButton.heightAnchor.constraint(equalToConstant: 50)]
        NSLayoutConstraint.activate(constraintsNavLeftButton)
        
        titleLabel.text = "Store Name Goes Here"
        titleLabel.font = UIFont(name: "SFProDisplay-Bold", size: 20)
        titleLabel.textColor = .white
        titleLabel.textAlignment = .center
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        headerView.addSubview(titleLabel)
        let constraintsTitleLabel = [titleLabel.leadingAnchor.constraint(equalTo: navLeftButton.trailingAnchor),
                                     titleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -50),
                                     titleLabel.bottomAnchor.constraint(equalTo: navLeftButton.bottomAnchor),
                                     titleLabel.centerYAnchor.constraint(equalTo: navLeftButton.centerYAnchor)]
        NSLayoutConstraint.activate(constraintsTitleLabel)
    }
    
    func changeTitleText() {
        titleLabel.text = titleText
    }
    
    func changeLeftButtonType() {
        switch navLeftButtonType {
        case leftButtonType.back?:
            let bImage = #imageLiteral(resourceName: "back").withRenderingMode(.alwaysTemplate)
            navLeftButton.tintColor = .white
            navLeftButton.setImage(bImage, for: .normal)
            navLeftButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        case leftButtonType.menu?:
            let mImage = #imageLiteral(resourceName: "menu").withRenderingMode(.alwaysTemplate)
            navLeftButton.tintColor = .white
            navLeftButton.setImage(mImage, for: .normal)
            navLeftButton.addTarget(self, action: #selector(showMenu), for: .touchUpInside)
        default:
            break
        }
    }
    
    @objc func backAction() {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func showMenu() {
        AppManager.commonMethods.showHamburgerMenu()
    }
    
    @objc func setNotificationTxtV() {
        notificationTxtV = UITextView(frame: .zero)
        notificationTxtV.layer.cornerRadius = 5.0
        notificationTxtV.layer.masksToBounds = true
        notificationTxtV.translatesAutoresizingMaskIntoConstraints = false
        notificationTxtV.font = UIFont(name: "SFProDisplay-Semibold", size: 21)!
        
        switch notificationTxtVColor {
        case .light?:
            notificationTxtV.backgroundColor = .white
            notificationTxtV.textColor = UIColor(hexCode: 0x424242FF)
        case .dark?:
            notificationTxtV.backgroundColor = UIColor(hexCode: 0x424242FF)
            notificationTxtV.textColor = .white
        default:
            notificationTxtV.backgroundColor = UIColor(hexCode: 0x424242FF)
            notificationTxtV.textColor = .white
        }
        
        view.addSubview(notificationTxtV)
        
        notificationTxtVBottom = notificationTxtV.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 50)
        notificationTxtVHeight = notificationTxtV.heightAnchor.constraint(equalToConstant: 30)
        let cons = [notificationTxtV.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 10),
                    notificationTxtVBottom!,
                    notificationTxtV.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -10),
                    notificationTxtVHeight!]
        NSLayoutConstraint.activate(cons)
    }
    
    func setNotificationTxtVColors(forColorScheme color: notificationColor) {
        
        switch color {
        case .light:
            notificationTxtV.backgroundColor = .white
            notificationTxtV.textColor = UIColor(hexCode: 0x424242FF)
        case .dark:
            notificationTxtV.backgroundColor = UIColor(hexCode: 0x424242FF)
            notificationTxtV.textColor = .white
        }
    }
    
    @objc func animateNotification(forTimeInterval timeInterval: TimeInterval, message: String) {
        
        notificationTxtV.text = message
        let h = AppManager.commonMethods.getHeightOfStringDarwin(forString: message, font: UIFont.systemFont(ofSize: 21, weight: .bold), width: currentScreenRect.width - 30)
        notificationTxtVHeight.constant = h < 35 ? 30 : h
        
        UIView.animate(withDuration: 0.57, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: .curveEaseIn, animations: {
            self.notificationTxtVBottom.constant = -40
            self.view.layoutIfNeeded()
        }) { _ in
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            UIView.animate(withDuration: 0.57, delay: timeInterval, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.9, options: .curveEaseOut, animations: {
                self.notificationTxtVBottom.constant = 50
                self.view.layoutIfNeeded()
            }, completion: { _ in
                
            })
        }
        
    }
}
