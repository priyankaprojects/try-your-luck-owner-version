//
//  ViewController.swift
//  Try Your Luck Owner
//
//  Created by Hitesh Agarwal on 20/12/18.
//  Copyright © 2018 Hitesh Agarwal. All rights reserved.
//

import UIKit

class LoadingScreenVC: UIViewController {

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBar?.backgroundColor = .white
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.asyncAfter(deadline: .now()+2) {
            self.checkFirstLaunch()
            let go = mainStoryboard.instantiateViewController(withIdentifier: "WalkThroughVC") as! WalkThroughVC
            self.present(go, animated: true, completion: nil)
        }
    }

    private func checkFirstLaunch() {
        if userDefaults.value(forKey: "FIRSTLAUNCH") as? String == nil {
            userDefaults.set("Some", forKey: "FIRSTLAUNCH")
            let go = mainStoryboard.instantiateViewController(withIdentifier: "GenerateBarcodeVC") as! GenerateBarcodeVC
            navigationController?.pushViewController(go, animated: false)
        } else {
            let go = mainStoryboard.instantiateViewController(withIdentifier: "SetAmountVC") as! SetAmountVC
            navigationController?.pushViewController(go, animated: false)
        }
    }
}

