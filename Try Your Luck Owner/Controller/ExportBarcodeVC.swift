//
//  ExportBarcodeVC.swift
//  Try Your Luck Owner
//
//  Created by Hitesh Agarwal on 24/12/18.
//  Copyright © 2018 Hitesh Agarwal. All rights reserved.
//

import UIKit

class ExportBarcodeVC: UIViewController {
    
    //MARK:- Outlate
    @IBOutlet weak var barCodeIV: UIImageView!
    @IBOutlet weak var saveBarcodeBtn: SubmitButton!
    
    //MARK:- VAR
    private var barCodeImageURL = AppManager.Barcode.barcodeImageURL
    private var barcodeImage = AppManager.Barcode.barcodeImage
    
    
    //MARK:- Action
    @IBAction func action_backBtn(_ sender: Any) {
        /*let nav = self.navigationController
        let go = mainStoryboard.instantiateViewController(withIdentifier: "SetAmountVC") as! SetAmountVC
        nav?.viewControllers.insert(go, at: 0)
        nav?.popToRootViewController(animated: true)*/
    }
    
    @IBAction func action_savebarcodeBtn(_ sender: Any) {
        //self.uploadBarcodeImage()
        let storeName = UserDefaults.standard.value(forKey: "storeName") as! String
        let owner_email = UserDefaults.standard.value(forKey: "email") as! String
        self.saveUserInformation(storeName: storeName, email: owner_email)
    }
    //For save barcode
    @objc private func saveBarcode() {
        if barCodeIV.image == nil {
            return
        }
        let fileManager = FileManager.default
        let imagePath = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(barcoodeImageName)
        //print(imagePath, "\n", AppManager.Barcode.barcodeImage as Any)
        let img = AppManager.Barcode.barcodeImage!
        let pngData = img.pngData()
        if fileManager.createFile(atPath: imagePath, contents: pngData!, attributes: nil) {
            AppManager.Barcode.barcodeImageURL = imagePath
            let okayAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            CommonMethods.shared.showAlert(withTitle: "Success", message: "Barcode saved as Image to gallery.", actions: [okayAction], OnViewController: self)
        } else {
            let okayAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            CommonMethods.shared.showAlert(withTitle: "Image Saving Error", message: "Could not save image to gallery. Please try again!", actions: [okayAction], OnViewController: self)
        }
    }
    
    
    //MARK:- Method
    override func viewDidLoad() {
        super.viewDidLoad()

        //titleText = "Your Barcode"
        //navLeftButtonType = .back
        //notificationTxtVColor = .light
        setBarcodeImage()
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    private func setBarcodeImage() {
        
        let fileManager = FileManager.default
        let imagePath = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(barcoodeImageName)
        if fileManager.fileExists(atPath: imagePath) {
            let i = UIImage(contentsOfFile: imagePath)
            barCodeIV.image = i
        } else {
            barCodeIV.image = barcodeImage
        }
        
    }
    
    //MARK:- API
    
    func uploadBarcodeImage(storeName: String, email: String){
        
        
        let compressedData = barcodeImage?.jpegData(compressionQuality: 0.25)
        let base64String = compressedData!.base64EncodedString(options: .lineLength64Characters)
        let param = "store_name=\(storeName)&owner_email=\(email)&barcode_image=\(base64String)"
        
        self.startActivity()
        APIManager.uploadImage(param: param, handler: {(data, message, error) in
            DispatchQueue.main.async {
                self.stopActivity()
                if error == nil {
                    //self.saveUserInformation(storeName: storeName, email: owner_email)
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SetAmountVC") as! SetAmountVC
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    self.showAlert(withTitle: "Error!", andMessage: error.debugDescription)
                }
            }
        })
        
    }
    
    func saveUserInformation(storeName: String, email: String){
        
        let deviceToken = UUID().uuidString
        self.startActivity()
        APIManager.saveOwnerDetails(device_token: deviceToken, store_name: storeName, owner_email: email, handler: {(data, message, error) in
            DispatchQueue.main.async {
                self.stopActivity()
                if error == nil{
                    if data != nil{
                        self.uploadBarcodeImage(storeName: storeName, email: email)
                        
                    }else{
                        self.showAlert(withTitle: "Message!", andMessage: message!)
                    }
                }else{
                    self.showAlert(withTitle: "Error!", andMessage: error?.localizedDescription)
                }
            }
        })
        
    }
    
    
}
