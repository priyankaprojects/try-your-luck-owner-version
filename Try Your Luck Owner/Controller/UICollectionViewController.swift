//
//  UICollectionViewController.swift
//  Try Your Luck Owner
//
//  Created by Hitesh Agarwal on 20/12/18.
//  Copyright © 2018 Hitesh Agarwal. All rights reserved.
//

import UIKit

@objc protocol tappedOnCollectionViewCell {
    @objc func cellSelected(withIndexPath: IndexPath)
    @objc func cellDeselected(withIndexPath: IndexPath)
    @objc optional func cellWillDisplay(forIndexPath indexPath: IndexPath)
}

class CollectionViewController: NSObject {

    public static let `default` = CollectionViewController()
    public var sectionNumber: Int?
    public var numberOfItemsInSection: [String:Int]?
    public var cellIdentifier: String!
    public var delegate: tappedOnCollectionViewCell?
    public var loadData: NSArray?
    public var cellSize: CGSize?
    public var minimumLineSpacing: CGFloat?
    public var minimumInteritemSpacing: CGFloat?
}

extension CollectionViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return sectionNumber == nil ? 0 : sectionNumber ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (sectionNumber == nil || loadData == nil) ? 0 : numberOfItemsInSection?["\(section)"] ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if cellIdentifier != nil {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! UICollectionViewCellController
            cell.loadData = self.loadData?[indexPath.row] as? NSDictionary
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
}

extension CollectionViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.cellSelected(withIndexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        delegate?.cellDeselected(withIndexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        delegate?.cellWillDisplay!(forIndexPath: indexPath)
    }
}

extension CollectionViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return cellSize ?? CGSize(width: 0, height: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return minimumLineSpacing ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return minimumInteritemSpacing ?? 0
    }
}
