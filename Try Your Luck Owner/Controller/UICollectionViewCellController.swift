//
//  UICollectionViewCellController.swift
//  Try Your Luck Owner
//
//  Created by Hitesh Agarwal on 20/12/18.
//  Copyright © 2018 Hitesh Agarwal. All rights reserved.
//

import UIKit

class UICollectionViewCellController: UICollectionViewCell {
    
    public var loadData: NSDictionary? {
        didSet {
            setupElements()
        }
    }
    
    func setupElements() {
        
    }
}
