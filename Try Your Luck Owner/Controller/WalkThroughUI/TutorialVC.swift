//
//  TutorialVC.swift
//  Try Your Luck Owner
//
//  Created by Hitesh on 02/01/19.
//  Copyright © 2019 Hitesh Agarwal. All rights reserved.
//

import UIKit

class TutorialVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    //MARK:- Outlate
    @IBOutlet weak var collView: UICollectionView!
    @IBOutlet weak var pageController: UIPageControl!
    @IBOutlet weak var view_collectionViewBack: UIView!
    
    
    
    //MARK:- VAR
    
    
    
    //MARK:- Action
    
    
    
    //MARK:- Method
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    
    //MARK:-Delegate & DataSource
    //Collection View Delegate & DataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TutorialCVCell", for: indexPath) as! TutorialCVCell
        
        cell.loadCell(itemNumber: indexPath.item, handler: {(data, message) in
            if message == "continue" || message == "skip"{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "GenerateBarcodeVC") as! GenerateBarcodeVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
        })
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageController.currentPage = indexPath.item
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view_collectionViewBack.frame.size.width, height: self.view_collectionViewBack.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
