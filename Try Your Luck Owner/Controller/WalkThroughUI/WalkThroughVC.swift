//
//  WalkThroughVC.swift
//  Try Your Luck Owner
//
//  Created by Hitesh Agarwal on 20/12/18.
//  Copyright © 2018 Hitesh Agarwal. All rights reserved.
//

import UIKit

class WalkThroughVC: MasterViewController {

    @IBOutlet weak var imageCV: UICollectionView!
    @IBOutlet weak var tutorialTitle: UILabel!
    @IBOutlet weak var tutorialText: UITextView!
    @IBOutlet weak var pgController: UIPageControl!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var continueButton: SubmitButton!
    
    var walkThroughElements: [WalkThrough.WalkThroughItems]!
    var data = DataParser.shared.tutorials
    
    override func viewDidLoad() {
        super.viewDidLoad()

        titleText = "Tutorials"
        navLeftButton.isHidden = true
        setupCollectionView()
        continueButton.alpha = 0.0
        continueButton.addTarget(self, action: #selector(removeSelf), for: .touchUpInside)
        skipButton.addTarget(self, action: #selector(removeSelf), for: .touchUpInside)
    }
    
    func setupCollectionView() {
        
        walkThroughElements = data
        AppManager.walkThrough.imageArrayTemp = data
        imageCV.dataSource = AppManager.collectionViewController
        imageCV.delegate = AppManager.collectionViewController
        
        var e: [NSDictionary] = []
        for i in data {
            e.append(i.walkThroughDict)
        }
        AppManager.collectionViewController.loadData = e as NSArray
        AppManager.collectionViewController.cellIdentifier = "walkThroughCell"
        AppManager.collectionViewController.sectionNumber = 1
        AppManager.collectionViewController.numberOfItemsInSection = ["0":AppManager.walkThrough.imageArrayTemp!.count]
        AppManager.collectionViewController.cellSize = CGSize(width: currentScreenRect.width, height: 180)
        AppManager.collectionViewController.delegate = self
    }
    
    func moveSkipButton() {
        UIView.transition(with: skipButton, duration: 0.57, options: .curveEaseIn, animations: {
            self.skipButton.alpha = 0.0
            self.continueButton.alpha = 1.0
        }) { _ in
            
        }
    }
    
    func moveSkipButtonReversed() {
        UIView.transition(with: skipButton, duration: 0.57, options: .curveEaseIn, animations: {
            self.skipButton.alpha = 1.0
            self.continueButton.alpha = 0.0
        }) { _ in
            
        }
    }
    
    @objc func removeSelf() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension WalkThroughVC: tappedOnCollectionViewCell {
    
    func cellSelected(withIndexPath: IndexPath) {
        
    }
    
    func cellDeselected(withIndexPath: IndexPath) {
        
    }
    
    func cellWillDisplay(forIndexPath indexPath: IndexPath) {
        pgController.currentPage = indexPath.item
        if indexPath.item == 2 {
            moveSkipButton()
        } else {
            moveSkipButtonReversed()
        }
    }
}
