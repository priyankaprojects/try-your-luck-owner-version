//
//  WalkThroughCVC.swift
//  Try Your Luck Owner
//
//  Created by Hitesh Agarwal on 20/12/18.
//  Copyright © 2018 Hitesh Agarwal. All rights reserved.
//

import UIKit

class WalkThroughCVC: UICollectionViewCellController {
    
    @IBOutlet weak var ImageV: UIImageView!
    
    override func setupElements() {
        ImageV.image = loadData?.value(forKey: "image") as? UIImage
    }
}
