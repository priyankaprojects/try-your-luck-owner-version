//
//  GenerateBarcodeVC.swift
//  Try Your Luck Owner
//
//  Created by Hitesh Agarwal on 20/12/18.
//  Copyright © 2018 Hitesh Agarwal. All rights reserved.
//

import UIKit

class GenerateBarcodeVC: UIViewController {
    //MARK:- Outlate
    @IBOutlet weak var storeNameTF: UITextField!
    @IBOutlet weak var emailAddressTF: UITextField!
    @IBOutlet weak var createBarcodeBtn: SubmitButton!
    
    //NARK:- VAR
    
    
    
    
    //MARK:- Action
    @objc func createBarcode() {
        
        if storeNameTF.text != nil &&
            storeNameTF.text != "" &&
            emailAddressTF.text != nil &&
            emailAddressTF.text != "" && isValidEmail(testStr: emailAddressTF.text!) {
            AppManager.storeName = storeNameTF.text!
            /*self.startActivity()
            DispatchQueue.main.asyncAfter(deadline: .now()+1.5) {
                AppManager.commonMethods.generateBarcode(from: self.storeNameTF.text!, completionHandler: { (finished, finalImage) in
                    self.stopActivity()
                    if finished {
                        AppManager.Barcode.barcodeImage = finalImage
                        let go = mainStoryboard.instantiateViewController(withIdentifier: "ExportBarcodeVC") as! ExportBarcodeVC
                        self.navigationController?.pushViewController(go, animated: true)
                    } else {
                        let okayAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                        CommonMethods.shared.showAlert(withTitle: "Something went wrong", message: "Could not create barcode from string. Please try again!", actions: [okayAction], OnViewController: self)
                    }
                })
            }*/
            
            self.fetchBarcodeImage(storeName: storeNameTF.text!, email: emailAddressTF.text!)
        } else {
            //let okayAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            //CommonMethods.shared.showAlert(withTitle: "Incomplete Form", message: "Please fill up all the details", actions: [okayAction], OnViewController: self)
            self.showAlert(withTitle: "Incomplete Form", andMessage: "Please fill up all the details properly")
        }
    }
    
    //MARK:- Method
    override func viewDidLoad() {
        super.viewDidLoad()

//        titleText = "Generate Barcode"
//        navLeftButton.isHidden = true
        
        storeNameTF.delegate = self
        emailAddressTF.delegate = self
        
        //Set terget Of createBarcode button
        createBarcodeBtn.addTarget(self, action: #selector(createBarcode), for: .touchUpInside)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    
    //MARK:- API
    func fetchBarcodeImage(storeName: String, email: String){
        self.startActivity()
        APIManager.fetchBarcode(storeName: storeName, email: email, handler: {(data, message, error) in
            DispatchQueue.main.async {
                self.stopActivity()
                if error == nil{
                    if data != nil && message == "Barcode Image has been retrived successfully."{
                        //Send to next Page
                        let imgUrl = data as! String
                        UserDefaults.standard.set(imgUrl, forKey: "barcodeUrl")
                        UserDefaults.standard.set(storeName, forKey: "storeName")
                        UserDefaults.standard.set(email, forKey: "email")
                        
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SetAmountVC") as! SetAmountVC
                        self.navigationController?.pushViewController(vc, animated: true)
                    }else{
                        //Generate Barcode
                        print(message!)
                        AppManager.commonMethods.generateBarcode(from: storeName, completionHandler: { (finished, finalImage) in
                            if finished {
                                AppManager.Barcode.barcodeImage = finalImage
                                
                                UserDefaults.standard.set(storeName, forKey: "storeName")
                                UserDefaults.standard.set(email, forKey: "email")
                                
                                let go = mainStoryboard.instantiateViewController(withIdentifier: "ExportBarcodeVC") as! ExportBarcodeVC
                                self.navigationController?.pushViewController(go, animated: true)
                                
                            } else {
                                
                                self.showAlert(withTitle: "Something went wrong!", andMessage: "Could not create barcode from string. Please try again!")
                            }
                        })
                    }
                }else{
                    self.showAlert(withTitle: "Error!", andMessage: error?.localizedDescription)
                }
            }
        })
    }
    
}

//MARK:- UITextFieldDelegate
extension GenerateBarcodeVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == storeNameTF {
            textField.resignFirstResponder()
            emailAddressTF.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
}
