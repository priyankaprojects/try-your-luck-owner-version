//
//  OfferHistoryVC.swift
//  Try Your Luck Owner
//
//  Created by Hitesh Agarwal on 20/12/18.
//  Copyright © 2018 Hitesh Agarwal. All rights reserved.
//

import UIKit

class OfferHistoryVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    //MARK:- Outlate
    @IBOutlet weak var historyTable: UITableView!
    
    
    //MARK:- VAR
    var offerHistoryData = [DataParser.shared.offers, DataParser.shared.offers, DataParser.shared.offers, DataParser.shared.offers, DataParser.shared.offers, DataParser.shared.offers, DataParser.shared.offers, DataParser.shared.offers, DataParser.shared.offers, DataParser.shared.offers]
    
    var offerListData = NSMutableArray()
    
    //MARK:- Action
    @IBAction func action_backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    //MARK:- Method
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getOfferList()
        
        /*
        titleText = "Offer History"
        navLeftButtonType = .back
        
        AppManager.offerHistory.offerHistoryItems = offerHistoryData
        
        var e: [NSDictionary] = []
        for i in offerHistoryData {
            e.append(i.histDict)
        }
        AppManager.tableController.loadData = e as NSArray
        AppManager.tableController.cellIdentifier = "OfferHistoryCell"
        AppManager.tableController.sectionNumber = 1
        AppManager.tableController.numberOfRowsInSection = ["0":AppManager.offerHistory.offerHistoryItems.count]
        AppManager.tableController.rowHeight = 130
        AppManager.tableController.delegate = self
        historyTable.dataSource = AppManager.tableController
        historyTable.delegate = AppManager.tableController
        historyTable.reloadData()
        */
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    //MARK:- Delegate & DataSource
    //Tableview Delegate & DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.offerListData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OfferTVCell", for: indexPath) as! OfferTVCell
        let model = offerListData[indexPath.row] as! OfferModel
        cell.loadCell(model: model, handler: {(data, message) in
            print(message)
        })
        
        return cell
    }
    
    
    //MARK: API
    func getOfferList(){
        self.startActivity()
        APIManager.fetchOffers(handler: { (data, message, error) in
            DispatchQueue.main.async {
                self.stopActivity()
                
                if error == nil{
                    if data != nil {
                        self.offerListData = data as! NSMutableArray
                        self.historyTable.reloadData()
                        
                    }else{
                        self.showAlert(withTitle: "Message!", andMessage: message)
                    }
                }else{
                    
                    self.showAlert(withTitle: "Error!", andMessage: error?.localizedDescription)
                }
            }
        })
    }
}

//extension OfferHistoryVC: tappedOnTableCell {
//    
//    func rowSelected(withIndexPath: IndexPath) {
//        
//    }
//    
//    func rowDeselected(withIndexPath: IndexPath) {
//        
//    }
//}
