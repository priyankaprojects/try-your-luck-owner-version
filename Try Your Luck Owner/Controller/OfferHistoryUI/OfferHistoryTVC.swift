//
//  OfferHistoryTVC.swift
//  Try Your Luck Owner
//
//  Created by Hitesh Agarwal on 20/12/18.
//  Copyright © 2018 Hitesh Agarwal. All rights reserved.
//

import UIKit

class OfferHistoryTVC: UITableViewCellController {

    @IBOutlet weak var offerValue: UILabel!
    @IBOutlet weak var appliedOn: UILabel!
    @IBOutlet weak var offerOn: UILabel!
    @IBOutlet weak var totalAmount: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setupElements() {
        offerValue.text = loadData?.value(forKey: "availedOfferValue") as? String
        appliedOn.text = loadData?.value(forKey: "offerApplied") as? String
        offerOn.text = loadData?.value(forKey: "offerOn") as? String
        totalAmount.text = loadData?.value(forKey: "total") as? String
    }
}
