//
//  BarCodeTVCell.swift
//  Try Your Luck Owner
//
//  Created by Hitesh on 03/01/19.
//  Copyright © 2019 Hitesh Agarwal. All rights reserved.
//

import UIKit
import SDWebImage

class BarCodeTVCell: UITableViewCell {
    //MARK:- Outlate
    @IBOutlet weak var img_barcode: UIImageView!
    
    
    
    //MARK:- VAR
    
    
    
    //MARK:- Action
    
    
    
    //MARK:- Method
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadCell(){
        /*let fileManager = FileManager.default
        let imagePath = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(barcoodeImageName)
        if fileManager.fileExists(atPath: imagePath) {
            let i = UIImage(contentsOfFile: imagePath)
            img_barcode.image = i
        } else {
            img_barcode.image = AppManager.Barcode.barcodeImage
        }*/
        let imgUrl = UserDefaults.standard.value(forKey: "barcodeUrl") as! String
        img_barcode.sd_setImage(with: URL(string: imgUrl), completed: nil)
        
    }

}
