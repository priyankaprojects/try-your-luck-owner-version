//
//  OfferTVCell.swift
//  Try Your Luck Owner
//
//  Created by Hitesh on 31/12/18.
//  Copyright © 2018 Hitesh Agarwal. All rights reserved.
//

import UIKit

class OfferTVCell: UITableViewCell {
    //MARK:- Outlate
    @IBOutlet weak var btn_addFund: SubmitButton!
    @IBOutlet weak var lbl_offerAvailed: UILabel!
    @IBOutlet weak var lbl_offerApplied: UILabel!
    @IBOutlet weak var lbl_offerDate: UILabel!
    @IBOutlet weak var lbl_totalOffer: UILabel!
    
    
    
    //MARK:- VAR
    typealias CellHandler = (_ data: Any?, _ message: String) -> Void
    var handler : CellHandler!
    
    
    //MARK:- Action
    @IBAction func action_addFundBtn(_ sender: Any) {
        handler(nil, "Add Fund btn clicked")
    }
    
    
    
    //MARK:- Method
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadCell(model: OfferModel, handler: @escaping CellHandler){
        self.handler = handler
        
        lbl_offerAvailed.text = "Availed offer value $\(model.availed_offer)"
        lbl_offerApplied.text = "Offer applied on \(model.offer_applied) persons"
        lbl_offerDate.text = "\(model.offer_date)\(model.offer_month) \(model.offer_year)"
        lbl_totalOffer.text = "$\(model.total_amount)"
        
        if model.today_offer == 1{
            self.btn_addFund.isHidden = false
            self.setBtn()
        }else{
            self.btn_addFund.isHidden = true
        }
    }
    
    
    func setBtn(){
        btn_addFund.layer.cornerRadius = 5.0
        
        btn_addFund.layer.shadowColor = UIColor.appPrimaryPurple.cgColor
        btn_addFund.layer.shadowOffset = CGSize(width: 0, height: 0)
        btn_addFund.layer.shadowRadius = 8
        btn_addFund.layer.shadowOpacity = 0.7
    }

}
