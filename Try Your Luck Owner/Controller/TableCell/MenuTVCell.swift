//
//  MenuTVCell.swift
//  Try Your Luck Owner
//
//  Created by Hitesh on 03/01/19.
//  Copyright © 2019 Hitesh Agarwal. All rights reserved.
//

import UIKit

class MenuTVCell: UITableViewCell {
    //MARK:- Outlate
    @IBOutlet weak var img_memuIcon: UIImageView!
    @IBOutlet weak var lbl_menuName: UILabel!
    
    
    
    //MARK:- VAR
    typealias CellHandler = (_ data: Any?, _ message: String) -> Void
    var handler : CellHandler!
    
    //MARK:- Action
    
    
    
    //MARK:- Method
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadCell(row: Int, handler: @escaping CellHandler){
        self.handler = handler
        switch row {
        case 0:
            img_memuIcon.image = UIImage(named: "history-ic")
            lbl_menuName.text = "My History"
        case 1:
            img_memuIcon.image = UIImage(named: "Terms")
            lbl_menuName.text = "Terms and condition"
        case 2:
            img_memuIcon.image = UIImage(named: "Privacy")
            lbl_menuName.text = "Privacy Policy"
        default:
            lbl_menuName.text = "Default"
        }
    }

}
