//
//  SetAmountVC.swift
//  Try Your Luck Owner
//
//  Created by Hitesh Agarwal on 20/12/18.
//  Copyright © 2018 Hitesh Agarwal. All rights reserved.
//

import UIKit

class SetAmountVC: UIViewController {
    //MARK: OUTLATE
    @IBOutlet weak var startOverView: UIView!
    @IBOutlet weak var startOverBtn: UIButton!
    @IBOutlet weak var txt_totalOfferAmount: UITextField!
    @IBOutlet weak var txt_maxOfferValue: UITextField!
    
    
    //MARK: VAR
    
    
    
    
    //MARK: Action
    @IBAction func action_menuBtn(_ sender: Any) {
        let go = mainStoryboard.instantiateViewController(withIdentifier: "HamburgerVC") as! HamburgerVC
//        let window = UIApplication.shared.keyWindow
//        window?.addSubview(go.view)
//        //self.view.window?.addSubview(go.view)
        go.modalPresentationStyle = .overCurrentContext
        go.modalTransitionStyle = .crossDissolve

        self.present(go, animated: true, completion: nil)
    }
    
    @IBAction func action_setAmountBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferHistoryVC") as! OfferHistoryVC
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    /*@objc func presentStartOverView() {
        
        startOverView.translatesAutoresizingMaskIntoConstraints = false
        startOverView.alpha = 0.0
        self.view.addSubview(startOverView)
        let cons = [startOverView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
                    startOverView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
                    startOverView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
                    startOverView.heightAnchor.constraint(equalToConstant: 95.0)]
        NSLayoutConstraint.activate(cons)
        UIView.animate(withDuration: 0.32) {
            self.startOverView.alpha = 1.0
        }
    }
    
    @objc func startOverAction() {
        AppManager.commonMethods.refreshApp()
        let nav = self.navigationController
        let loading = mainStoryboard.instantiateViewController(withIdentifier: "LoadingScreenVC") as! LoadingScreenVC
        nav?.viewControllers.insert(loading, at: 0)
        nav?.popToRootViewController(animated: false)
    }*/
    
    
    //MARK: Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txt_maxOfferValue.layer.cornerRadius = 5.0
        txt_maxOfferValue.layer.borderWidth = 1.0
        txt_maxOfferValue.layer.borderColor = UIColor.black.cgColor
        
        txt_totalOfferAmount.layer.cornerRadius = 5.0
        txt_totalOfferAmount.layer.borderWidth = 1.0
        txt_totalOfferAmount.layer.borderColor = UIColor.black.cgColor
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
}
