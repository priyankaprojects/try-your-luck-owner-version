
//
//  CALayer+Storyboard.swift
//  Late Lateef
//
//  Created by Sandip on 04/01/19.
//  Copyright © 2019 Sandip. All rights reserved.
//

import Foundation

import UIKit

extension CALayer {
    
    func setBorderColorFromUIColor(_ color: UIColor) {
        self.borderColor = color.cgColor
    }
}
