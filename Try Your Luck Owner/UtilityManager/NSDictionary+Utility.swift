//
//  NSDictionary+Utility.swift
//  DNADemo
//
//  Created by Sandip on 04/01/19.
//  Copyright © 2019 Sandip. All rights reserved.
//

import Foundation

extension NSDictionary {
    
    func toStringFrom(key akey: String) -> String {
        if let str = self[akey] as? String {
            return str
        } else {
            return ""
        }
    }
    
    func toNSArrayFrom(key aKey: String) -> NSArray {
        if let arr = self[aKey] as? NSArray {
            return arr
        } else {
            return []
        }
    }
    
    func toBoolFrom(key akey: String) -> Bool {
        if let bool = self[akey] as? Bool {
            return bool
        } else {
            return false
        }
    }
    func toIntFrom(key akey: String) -> Int {
        if let IntNumber = self[akey] as? Int {
            return IntNumber
        } else {
            return 0
        }
    }
    
    func toFloatFrom(key akey: String) -> Float {
        if let float = self[akey] as? Float {
            return float
        } else {
            return 0.0
        }
    }
    
    func toDoubleFrom(key akey: String) -> Double {
        if let double = self[akey] as? Double {
            return double
        } else {
            return 0.00
        }
    }
 
 
    
}
