//
//  UIView+AnimateUp.swift
//  Odd
//
//  Created by Sandip on 04/01/19.
//  Copyright © 2019 Sandip. All rights reserved.
//

import Foundation
import UIKit

/*extension UIView {

    func animateUpToTargetYOrigin(with yValue: CGFloat) {
        
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardDidChangeFrame, object: nil, queue: OperationQueue.main) { (note) in
            
            var yOrigin : CGFloat?
            yOrigin = 0
            let keyboardOr = (note.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.origin.y
             if yValue > keyboardOr! {
                yOrigin = yValue - keyboardOr!
            }
            
            UIView.animate(withDuration: 0.3, animations: {
                    self.frame = CGRect(x: self.frame.origin.x, y: -yOrigin!, width: self.frame.size.width, height: self.frame.size.height)

                self.layoutIfNeeded()
            })
            
            
            
        }
        
    }
    
    //For Round Corner Radius of The View/*S19062018S*/
    func roundCornerRadius(){//}(view: UIView){
        
        self.layer.cornerRadius = self.frame.size.width / 2
        self.layer.masksToBounds = true
    }
    
    //For Normal Corner Radious of The View/*S19062018S*/
    func normalCornerRadius(rediusValue: Float){ //view: UIView,
        
        self.layer.cornerRadius = CGFloat(rediusValue)
        self.layer.masksToBounds = true
    }
    
    //For Set Border With Color/*S19062018S*/
    func setBorderWithColor(width: Float, color: UIColor){
        self.layer.borderWidth = CGFloat(width)
        self.layer.setBorderColorFromUIColor(color)
    }
    
    //For Create Shadow of a view /*S31072018S*/
    func createShadow(color: UIColor, opacity: Float, offSet: CGSize, radius: CGFloat, scale: Bool) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    
}*/




/*
 
 - (void)animateUpToTargetYOrigin:(CGFloat)yValue
 {
 
 [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardDidChangeFrameNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
 CGFloat yOrigin = 0;
 CGFloat keyboardOr = [[[note userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].origin.y;
 if (yValue > keyboardOr) {
 yOrigin = yValue - keyboardOr;
 }
 [UIView animateWithDuration:0.3f animations:^{
 //            if (up) {
 self.frame = CGRectMake(self.frame.origin.x, -yOrigin, self.frame.size.width, self.frame.size.height);
 //            }
 //            else
 //            {
 //                self.frame = CGRectMake(self.frame.origin.x, 0, self.frame.size.width, self.frame.size.height);
 //            }
 
 }];
 
 // NSLog(@"%f  yo yo", [[[note userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].origin.y);
 }];
 
 
 
 }
 
 */
