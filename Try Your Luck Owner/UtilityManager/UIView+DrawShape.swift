//
//  UIView+DrawShape.swift
//  Caree
//
//  Created by Sandip on 04/01/19.
//  Copyright © 2019 Sandip. All rights reserved.
//

import UIKit

class UIView_DrawShape: UIView {

    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        guard let context = UIGraphicsGetCurrentContext() else { return }
        let h = rect.size.height
        let w = rect.size.width
        
        context.beginPath()
        context.move(to: CGPoint(x: rect.minX, y: rect.minY))
        context.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))
        context.addLine(to: CGPoint(x: (rect.maxX / 2), y: (rect.maxY / 2)))
        context.closePath()
        
        context.setFillColor(UIColor.white.cgColor)
        context.fillPath()
    }
 

}
