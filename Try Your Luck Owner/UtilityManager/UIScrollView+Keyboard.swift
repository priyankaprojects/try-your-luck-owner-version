//
//  UIScrollView+Keyboard.swift
//  Late Lateef
//
//  Created by Sandip on 04/01/19.
//  Copyright © 2019 Sandip. All rights reserved.
//

import Foundation
import UIKit

/*extension UIScrollView {
    func scrollContentForViewFrame(activeFrame: CGRect, andParentView parentView: UIView) {
        weak var showObserver: AnyObject?
        weak var hideObserver: AnyObject?
        showObserver = NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardDidShow, object: nil, queue: OperationQueue.main, using: {(note: Notification) -> Void in

            let kbSize = (note.userInfo![UIKeyboardFrameEndUserInfoKey] as! CGRect).size
            let contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0)
            self.contentInset = contentInsets
            self.scrollIndicatorInsets = contentInsets
            var aRect = parentView.frame
            aRect.size.height -= kbSize.height
            let aOrigin = CGPoint(x: activeFrame.origin.x, y: activeFrame.origin.y + activeFrame.size.height)
            
            if !aRect.contains(aOrigin) {
                self.scrollRectToVisible(activeFrame, animated: true)
            }
            else {
                self.scrollRectToVisible(CGRect.zero, animated: true)
            }
            NotificationCenter.default.removeObserver(showObserver as Any)
        })
        hideObserver = NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardDidHide, object: nil, queue: OperationQueue.main, using: {(note: Notification)  in
            let contentInsets = UIEdgeInsets.zero
            self.contentInset = contentInsets
            self.scrollIndicatorInsets = contentInsets
            NotificationCenter.default.removeObserver(hideObserver as Any)
        })
    }
    
}*/
//MARK: TextField Extension
extension UITextField {
    //for show done button on top of the keyboard
    func showDoneButtonOnKeyboard(buttonTintColor: UIColor) {
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(resignFirstResponder))
        //doneButton.tintColor = UIColor.init(hex: 0xFD5739)
        doneButton.tintColor = buttonTintColor
        
        var toolBarItems = [UIBarButtonItem]()
        toolBarItems.append(flexSpace)
        toolBarItems.append(doneButton)
        
        let doneToolbar = UIToolbar()
        doneToolbar.items = toolBarItems
        doneToolbar.sizeToFit()
        doneToolbar.backgroundColor = UIColor.white
        
        inputAccessoryView = doneToolbar
    }
}

