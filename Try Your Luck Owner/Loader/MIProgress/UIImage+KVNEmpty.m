//
//  UIImage+Empty.m
//  KVNProgress
//
//  Created by Mohammad Ishtiyak on 20/04/18.
//  Copyright (c) 2014 Tech Exactly. All rights reserved.
//

#import "UIImage+KVNEmpty.h"

@implementation UIImage (KVNEmpty)

+ (UIImage *)emptyImage
{
	return [[UIImage alloc] init];
}

@end
