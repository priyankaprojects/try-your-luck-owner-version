//
//  KVNRotationViewController.m
//  KVNProgress
//
//  Created by Mohammad Ishtiyak on 20/04/18.
//  Copyright (c) 2014 Tech Exactly. All rights reserved.
//

#import "KVNRotationViewController.h"

/**
 This class is only used to handle rotation automatically with a custom UIWindow
 http://stackoverflow.com/a/27091111/2571566
 */
@implementation KVNRotationViewController

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
	return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotate
{
	return YES;
}

@end
