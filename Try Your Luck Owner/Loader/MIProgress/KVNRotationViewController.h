//
//  KVNRotationViewController.h
//  KVNProgress
//
//  Created by Mohammad Ishtiyak on 20/04/18.
//  Copyright (c) 2014 Tech Exactly. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 This class is only used to handle rotation automatically with a custom UIWindow
 http://stackoverflow.com/a/27091111/2571566
 */
@interface KVNRotationViewController : UIViewController

@end
