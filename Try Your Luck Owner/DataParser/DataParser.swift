//
//  DataParser.swift
//  Try Your Luck Owner
//
//  Created by Hitesh Agarwal on 20/12/18.
//  Copyright © 2018 Hitesh Agarwal. All rights reserved.
//

import UIKit

class DataParser: NSObject {
    
    public static let shared = DataParser()
    
    //Static Data
    var offers = OfferHistoryData.OHistory(availedOfferValue: "Availed offer value $10", offerApplied: "Offer applied on 10 persons", offerOn: "Offer on Thu, 22nd Dec 2018", total: "$30")
    
    var tutorials = [WalkThrough.WalkThroughItems(image: #imageLiteral(resourceName: "generate-barcode")), WalkThrough.WalkThroughItems(image: #imageLiteral(resourceName: "print-baecode")), WalkThrough.WalkThroughItems(image: #imageLiteral(resourceName: "set-offer"))]
}
