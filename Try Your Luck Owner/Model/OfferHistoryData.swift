//
//  OfferHistoryData.swift
//  Try Your Luck Owner
//
//  Created by Hitesh Agarwal on 20/12/18.
//  Copyright © 2018 Hitesh Agarwal. All rights reserved.
//

import UIKit

class OfferHistoryData {
    
    public static let shared = OfferHistoryData()
    
    public struct OHistory {
        var availedOfferValue: String
        var offerApplied: String
        var offerOn: String
        var total: String
        
        var dict: [String:Any] {
            return ["availedOfferValue"   :  availedOfferValue,
                    "offerApplied"        :  offerApplied,
                    "offerOn"             :  offerOn,
                    "total"               :  total]
        }
        
        var histDict: NSDictionary {
            return dict as NSDictionary
        }
    }
    
    public var offerHistoryItems: [OHistory] = []
}
