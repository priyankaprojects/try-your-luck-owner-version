//
//  AppManager.swift
//  Try Your Luck Owner
//
//  Created by Hitesh Agarwal on 20/12/18.
//  Copyright © 2018 Hitesh Agarwal. All rights reserved.
//

import Foundation

class AppManager {
    
    static let `default` = AppManager()
    static let Barcode = BarcodeData.shared
    static let commonMethods = CommonMethods.shared
    static let tableController = TableViewController.default
    static let hamburger = Hamburger.shared
    static let offerHistory = OfferHistoryData.shared
    static let collectionViewController = CollectionViewController.default
    static let walkThrough = WalkThrough.shared
    static var storeName: String = ""
    static var appState: AppState = .continue
    
    
    enum AppState {
        case refresh
        case `continue`
    }
}
