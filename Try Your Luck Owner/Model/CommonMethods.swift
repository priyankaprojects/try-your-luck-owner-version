//
//  CommonMethods.swift
//  Try Your Luck Owner
//
//  Created by Hitesh Agarwal on 20/12/18.
//  Copyright © 2018 Hitesh Agarwal. All rights reserved.
//

import UIKit

class CommonMethods {
    
    static let shared = CommonMethods()
    
    @objc static func safeAreaTopHeight() -> CGFloat {
        let window = UIApplication.shared.keyWindow
        let topPadding = window?.safeAreaInsets.top
        return topPadding ?? 20
    }
    
    @objc func showAlert(withTitle T: String?, message M: String?, actions: [UIAlertAction], OnViewController VC: UIViewController) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: T, message: M, preferredStyle: .alert)
            for i in actions {
                alert.addAction(i)
            }
            VC.present(alert, animated: true, completion: nil)
        }
    }
    
    @objc func showHamburgerMenu() {
        let go = mainStoryboard.instantiateViewController(withIdentifier: "HamburgerVC") as! HamburgerVC
        let window = UIApplication.shared.keyWindow
        window?.addSubview(go.view)
    }
    
    func generateBarcode(from string: String, completionHandler: @escaping (_ finished: Bool, _ image: UIImage?) -> ()) {
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CICode128BarcodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 1, y: 1)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                let context:CIContext = CIContext.init(options: nil)
                let cgImage:CGImage = context.createCGImage(output, from: output.extent)!
                let rawImage:UIImage = UIImage.init(cgImage: cgImage)
                
                let cgimage: CGImage = (rawImage.cgImage)!
                let cropZone = CGRect(x: 0, y: 0, width: Int(rawImage.size.width), height: Int(rawImage.size.height))
                let cWidth: size_t  = size_t(cropZone.size.width)
                let cHeight: size_t  = size_t(cropZone.size.height)
                let bitsPerComponent: size_t = cgimage.bitsPerComponent
                
                let bytesPerRow = (cgimage.bytesPerRow) / (cgimage.width  * cWidth)
                
                let context2: CGContext = CGContext(data: nil, width: cWidth, height: cHeight, bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: CGColorSpaceCreateDeviceRGB(), bitmapInfo: cgimage.bitmapInfo.rawValue)!
                
                context2.draw(cgimage, in: cropZone)
                
                let result: CGImage  = context2.makeImage()!
                let finalImage = UIImage(cgImage: result)
                
                completionHandler(true, finalImage)
            } else {
                completionHandler(false, nil)
            }
        } else {
            completionHandler(false, nil)
        }
    }
    
    func getHeightOfStringDarwin(forString str: String, font: UIFont, width: CGFloat) -> CGFloat {
        
        let textStorage = NSTextStorage(string: str)
        let textContainer = NSTextContainer(size: CGSize(width: width, height: CGFloat.greatestFiniteMagnitude))
        let layoutManager = NSLayoutManager()
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        textStorage.addAttribute(.font, value: font, range: NSRange(location: 0, length: textStorage.length))
        textContainer.lineFragmentPadding = 0.0
        layoutManager.glyphRange(for: textContainer)
        return layoutManager.usedRect(for: textContainer).size.height
    }
    
    public func refreshApp() {
        userDefaults.set(nil, forKey: "FIRSTLAUNCH")
        AppManager.Barcode.barcodeImage = nil
        AppManager.Barcode.barcodeImageURL = nil
    }
}
