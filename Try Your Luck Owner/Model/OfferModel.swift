//
//  OfferModel.swift
//  Try Your Luck Owner
//
//  Created by Hitesh on 04/01/19.
//  Copyright © 2019 Hitesh Agarwal. All rights reserved.
//

import UIKit

class OfferModel: NSObject {
    var today_offer = Int()
    var offer_set_id = ""
    var total_amount = ""
    var availed_offer = ""
    var offer_week_day = ""
    var offer_date = ""
    var offer_month = ""
    var offer_year = ""
    var offer_applied = ""
    
    convenience init(dict: NSDictionary) {
        self.init()
        
        today_offer = dict.toIntFrom(key: "today_offer")//value(forKey: "today_offer") as! Int
        offer_set_id = dict.toStringFrom(key: "offer_set_id")
        total_amount = dict.toStringFrom(key: "total_amount")
        availed_offer = dict.toStringFrom(key: "availed_offer")
        offer_week_day = dict.toStringFrom(key: "offer_week_day")
        offer_date = dict.toStringFrom(key: "offer_date")
        offer_month = dict.toStringFrom(key: "offer_month")
        offer_year = dict.toStringFrom(key: "offer_year")
        offer_applied = dict.toStringFrom(key: "offer_applied")
        
        
    }
}
