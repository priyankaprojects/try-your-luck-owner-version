//
//  WlkThroughData.swift
//  Try Your Luck Owner
//
//  Created by Hitesh Agarwal on 20/12/18.
//  Copyright © 2018 Hitesh Agarwal. All rights reserved.
//

import UIKit

class WalkThrough {
    
    public static let shared = WalkThrough()
    
    struct WalkThroughItems {
        var image: UIImage
        
        var dict: [String:Any] {
            return ["image": image]
        }
        
        var walkThroughDict: NSDictionary {
            return dict as NSDictionary
        }
    }
    
    public var imageArrayTemp: [WalkThroughItems]?
}
