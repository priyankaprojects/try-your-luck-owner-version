import UIKit

let currentScreenRect  =   UIScreen.main.bounds
let currentDevice      =   UIDevice.current
///Returns the model, i.e., iPhone, iPad, Macbook etc.
let currentDeviceModel =   UIDevice.current.model
let currentDeviceID    =   UIDevice.current.identifierForVendor?.uuidString
let currentAppVersion  =   Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "0.1"
let userDefaults       =   UserDefaults.standard
let mainStoryboard     =   UIStoryboard(name: "Main", bundle: Bundle.main)
let barcoodeImageName  =   "TRY_YOUR_LUCK_BARCODE"
