//
//  Hamburger.swift
//  Try Your Luck Owner
//
//  Created by Hitesh Agarwal on 20/12/18.
//  Copyright © 2018 Hitesh Agarwal. All rights reserved.
//

import UIKit

class Hamburger {
    
    static let shared = Hamburger()
    
    struct HamburgerItems {
        var id: String
        var menuName: String
        var menuImage: UIImage
        
        var dict: [String:Any] {
            return ["id"         :  id,
                    "menuName"   :  menuName,
                    "menuImage"  :  menuImage]
        }
        
        var hamDict: NSDictionary {
            return dict as NSDictionary
        }
    }
    
    public var hamburgerItems: [HamburgerItems] = []
}
