//
//  BarcodeData.swift
//  Try Your Luck Owner
//
//  Created by Hitesh Agarwal on 20/12/18.
//  Copyright © 2018 Hitesh Agarwal. All rights reserved.
//

import UIKit

class BarcodeData {
    
    static let shared = BarcodeData()
    var barcodeImageURL: String?
    var barcodeImage: UIImage?
}
